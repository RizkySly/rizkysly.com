<div class="privacy">
<h3>De eigenaar van de app is:</h3>
<p>Wouter van de Velde, werkend onder de naam Rizkysly, eenmanszaak.<br><br>
    <em>Sartrezijde 31<br>
        2725 PM Zoetermeer<br>
        Nederland<br>
        <a href="mailto:wvandevelde@rizkysly.com">wvandevelde@rizkysly.com</a><br>
        KvK: 27279829</em></p>
<h3>Welke informatie wordt verzameld?</h3>
<p>
<?php if ($this->push_notifications) : ?>
    <p>
        <strong>Push notificaties</strong><br>
        Tokens om notificaties te kunnen verzenden.
    </p>
<?php endif; ?>
<?php if ($this->statistics) : ?>
    <p>
    <strong>Statistieken</strong><br>
    Statistieken over gebruik (weergaven, acties, bezoeken) en apparaat informatie (merk, model, landinstelling, besturingssysteem en gebruikte applicatie). Geen persoonlijke gegevens.
    </p>
<?php endif; ?>
<?php if ($this->google_analystics) : ?>
    <p>
    <strong>Google Analytics (Alleen website)</strong><br>
    De website gebruikt Google Analytics om het bezoekersgedrag te analyseren en te verbeteren. Zie <a href="https://policies.google.com/privacy">Googles Privacy Policy</a> voor meer informatie.
</p>
<?php endif; ?>
<?php if ($this->google_adsense) : ?>
<p>
    <strong>Google Adsense (Alleen website)</strong><br>
    Google Adsense wordt gebruikt om persoonlijke advertenties te tonen. Zie <a href="https://policies.google.com/privacy">Googles Privacy Policy</a> voor meer informatie.
</p>
<?php endif; ?>
<h3>Wat is de juridische basis voor het verzamelen?</h3>
<p>De verzamelde informatie is nodig om de services die de app biedt uit te kunnen voeren en voor onderhoud en verbetering van de app.</p>
<h3>Welke derde partijen hebben toegang tot deze informatie?</h3>
<?php if ($this->google_analystics || $this->google_adsense) : ?>
<p>Anders dan Google voor AdSense and Analytics; Geen.</p>
<?php else : ?>
<p>Geen.</p>
<?php endif; ?>
<h3>Ingangsdatum van het privacybeleid</h3>
<p><?php echo $this->date; ?></p>
</div>