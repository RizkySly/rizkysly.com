<div class="privacy">
<h3>Der Besitzer der App ist:</h3>
<p>Wouter van de Velde, als Rizkysly tätig, ein Einzelunternehmen.<br><br>
    <em>
        Sartrezijde 31<br>
        2725 PM Zoetermeer<br>
        Die Niederlande<br>
        <a href="mailto:wvandevelde@rizkysly.com">wvandevelde@rizkysly.com</a><br>
        KvK: 27279829
    </em>
</p>
<h3>Welche Informationen werden gesammelt?</h3>
<p>
<?php if ($this->push_notifications) : ?>
    <p>
        <strong>Benachrichtigungen</strong><br>
        Token zum Senden von Benachrichtigungen.
    </p>
<?php endif; ?>
<?php if ($this->statistics) : ?>
    <p>
    <strong>Statistiken</strong><br>
    Nutzungsstatistiken (Ansichten, Aktionen, Besuche) und Geräteinformationen (Marke, Modell, Gebietsschema, Betriebssystem und Anwendung). Keine personenbezogenen Daten
    </p>
<?php endif; ?>
<?php if ($this->google_analystics) : ?>
    <p>
    <strong>Google Analytics (Nur Website)</strong><br>
    Die Website verwendet Google Analytics, um das Besucherverhalten zu analysieren und zu verbessern. Weitere Informationen finden Sie in den  <a href="https://policies.google.com/privacy">Google-Datenschutzbestimmungen</a>.
</p>
<?php endif; ?>
<?php if ($this->google_adsense) : ?>
<p>
    <strong>Google Adsense (Nur Website)</strong><br>
    Google Adsense wird verwendet, um personalisierte Werbung zu schalten. Weitere Informationen finden Sie in den  <a href="https://policies.google.com/privacy">Google-Datenschutzbestimmungen</a>.
</p>
<?php endif; ?>
<h3>Was ist die Rechtsgrundlage für die Sammlung?</h3>
<p>Die gesammelten Informationen sind für den Service der App sowie für die Pflege und Verbesserung der App erforderlich.</p>
<h3>Welche dritten Parteien haben Zugriff auf die Informationen?</h3>
<?php if ($this->google_analystics || $this->google_adsense) : ?>
<p>Abgesehen von den AdSense- und Analytics-Diensten von Google; Keiner.</p>
<?php else : ?>
<p>Keiner.</p>
<?php endif; ?>
<h3>Datum des Inkrafttretens der Datenschutzerklärung</h3>
<p><?php echo $this->date; ?></p>
</div>