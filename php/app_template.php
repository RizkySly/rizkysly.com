<div class="app">
<div class="buttons">
<?php foreach ($this->buttons as $button => $link) : ?>
	<a href="<?php echo $link; ?>"><img class="button" src="<?php echo $button; ?>" alt="Button"></a>
<?php endforeach; ?>
</div>
<div class="screens">
<?php foreach ($this->screens as $screen) : ?>
<img class="screen" src="<?php echo $screen; ?>" alt="Screenshot">
<?php endforeach; ?>
</div>
</div>