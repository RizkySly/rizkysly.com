<?php

class app
{
    private $buttons = [];
    private $screens = [];

    public function __construct()
    {
        $this->buttons = engine::$item['buttons'];
        $this->screens = engine::$item['screens'];
    }

    function view()
    {
        include(__DIR__ . '/app_template.php');
    }
}