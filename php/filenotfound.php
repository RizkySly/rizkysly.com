<?php

class filenotfound
{
    private $content = [];
    private $alias = '';
    private $app = '';

    public function __construct()
    {
        $this->content = engine::$locale['rizkysly']['filenotfound'];
        $this->alias = engine::$alias;
        $this->app = engine::$content['title'];
    }

    function view()
    {
        include(__DIR__ . '/filenotfound_template.php');
    }
}