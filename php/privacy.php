<?php

class privacy
{
    protected $date = '';
    protected $push_notifications = false;
    protected $statistics = false;
    protected $google_analystics = false;
    protected $google_adsense = false;

    public function __construct()
    {
        $this->date = strftime('%A %e %B %G', strtotime(engine::$item['date']));
        $this->push_notifications = isset(engine::$item['push_notifications']);
        $this->statistics = isset(engine::$item['statistics']);
        $this->google_analystics = isset(engine::$item['google_analystics']);
        $this->google_adsense = isset(engine::$item['google_adsense']);
    }

    protected function view()
    {
    }
}