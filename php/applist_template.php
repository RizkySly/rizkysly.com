<div class="applist">
<?php foreach ($this->apps as $app) : ?>
<a href="<?php echo $app['url']; ?>">
    <img class="icon" src="<?php echo $app['icon']; ?>">
    <div>
        <h3><?php echo $app['title']; ?></h3>
        <p><?php echo $app['description']; ?></p>
    </div>
</a>
<?php endforeach; ?>
</div>