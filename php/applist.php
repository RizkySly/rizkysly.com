<?php

class applist
{
    private $apps = [];

    public function __construct()
    {
        foreach (engine::$locale as $alias => $item) {
            if ($alias == 'rizkysly') {
                continue;
            }
            $this->apps[] = [
                'url' => '/' . $alias . '/',
                'title' => $item['title'],
                'description' => $item['description'],
                'icon' => $item['icon']
            ];
        }
    }

    function view()
    {
        include(__DIR__ . '/applist_template.php');
    }
}