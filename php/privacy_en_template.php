<div class="privacy">
<h3>The owner of the app is:</h3>
<p>Wouter van de Velde, operating as Rizkysly, a sole proprietorship.<br><br>
    <em>Sartrezijde 31<br>
        2725 PM Zoetermeer<br>
        The Netherlands<br>
        <a href="mailto:wvandevelde@rizkysly.com">wvandevelde@rizkysly.com</a><br>
        KvK: 27279829</em></p>
<h3>What information is being collected?</h3>
<p>
<?php if ($this->push_notifications) : ?>
    <p>
        <strong>Push notifications</strong><br>
        Tokens to send notifications.
    </p>
<?php endif; ?>
<?php if ($this->statistics) : ?>
    <p>
    <strong>Statistics</strong><br>
    Usage statistics (views, actions, visits) and device information (make, model, locale, operating
    system and application). No personal data.
    </p>
<?php endif; ?>
<?php if ($this->google_analystics) : ?>
    <p>
    <strong>Google Analytics (Website only)</strong><br>
    The website uses Google Analytics to analyze and improve visitor behavior. See <a href="https://policies.google.com/privacy">Googles Privacy Policy</a> for more information.
</p>
<?php endif; ?>
<?php if ($this->google_adsense) : ?>
<p>
        <strong>Google Adsense (Website only)</strong><br>
    Google Adsense is used to serve personalized advertising. See <a href="https://policies.google.com/privacy">Googles Privacy Policy</a> for more information.
</p>
<?php endif; ?>
<h3>What is the Legal basis for the collection?</h3>
<p>The information collected is necessary for the service the app provides and for maintaining and improving the app.</p>
<h3>Which third parties will have access to the information?</h3>
<?php if ($this->google_analystics || $this->google_adsense) : ?>
<p>Apart from Google's AdSense and Analytics services; None.</p>
<?php else : ?>
<p>None.</p>
<?php endif; ?>
<h3>Effective date of the privacy policy</h3>
<p><?php echo $this->date; ?></p>
</div>