<?php

class engine
{
    public static $domains = ['rizkysly.com', 'de.rizkysly.com', 'nl.rizkysly.com'];

    public static $language_code = 'en_US';
    public static $alias = 'rizkysly';
    public static $title = '';
    public static $icon = '';
    public static $menu = [];
    public static $item = [];

    private static $template = '';
    public static $content = [];
    public static $locale = [];
    private static $app = [];

    public static function init()
    {
        // Domain should be in the domains array and the locale file should exist
        if (!in_array($_SERVER['HTTP_HOST'], self::$domains)
            || !is_file(__DIR__ . '/../locale/' . $_SERVER['HTTP_HOST'] . '.json')) {
            ob_end_clean();
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: https://' . self::$domains[0]);
            exit;
        }

        // Load the locale data for the domain
        self::$locale = json_decode(file_get_contents(__DIR__ . '/../locale/' . $_SERVER['HTTP_HOST'] . '.json'), true);

        self::$language_code = self::$locale['rizkysly']['locale'];
        setlocale(LC_ALL, self::$language_code . '.utf8');

        // If something terrible happend
        if (json_last_error() != JSON_ERROR_NONE) {
            die('Locale JSON is invalid: error -> ' . json_last_error() . ', link ' . __DIR__ . '/../locale/' . $_SERVER['HTTP_HOST']);
        }

        // What app to show
        $uri = $_SERVER['REQUEST_URI'];
        $parts = explode('/', $uri);
        $partIndex = 1;
        if (count($parts) > 1 && array_search($parts[1], array_keys(self::$locale)) !== false) {
            self::$alias = $parts[1];
            $partIndex = 2;
        }

        // Get some app specific stuff 
        self::$content = self::$locale[self::$alias];
        self::$title = self::$content['title'];
        self::$icon = self::$content['icon'];
        self::$menu = self::$content['menu'];

        // What menu should be displayed
        if ($partIndex >= count($parts) || strlen(trim($parts[$partIndex])) == 0) {
            self::$item = self::$menu[0];
            self::$template = self::$item['template'];
        } elseif ($index = array_search($parts[$partIndex], array_map(function ($item) {
            return $item['alias'];
        }, self::$menu))) {
            self::$item = self::$menu[$index];
            self::$template = self::$item['template'];
        } else {
            self::$item = ['title' => self::$locale['rizkysly']['filenotfound']['title'], 'description' => ''];
            self::$template = 'filenotfound';
            http_response_code(404);
        }

        // Formati the description if it's in there
        if (strlen(trim(self::$item['description'])) > 0) {
            self::$item['description'] = '<p>' . nl2br(self::$item['description']) . '</p>';
        }
    }

    public static function alias(string $alias, bool $uri = false) : string
    {
        $alias = strtolower($alias);
        $alias = str_replace(' ', '-', $alias);
        $alias = str_replace('.', '-', $alias);

        if (strlen($alias) == 1) {
            $alias = '';
        } else {
            $alias .= '/';
        }

        if (!$uri) {
            return $alias;
        }

        if (self::$alias == 'rizkysly') {
            return '/' . $alias;
        }

        return '/' . self::$alias . '/' . $alias;
    }

    public static function load()
    {

        if (is_file(__DIR__ . '/' . self::$template . '.php')) {
            include_once(__DIR__ . '/' . self::$template . '.php');
            $view = new self::$template();
            return $view->view();
        }


    }

}