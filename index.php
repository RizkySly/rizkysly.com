<?php 
include(__DIR__ . '/php/engine.php');
engine::init();

?><!DOCTYPE html>
<html lang="<?php echo engine::$language_code; ?>">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo engine::$item['title'] . ' - ' . engine::$title; ?></title>
<link href="/css/screen.css" rel="stylesheet">
<link href="/css/<?php echo engine::$alias; ?>.css" rel="stylesheet">
</head>
<body>
<div class="header center">
<a href="/">
<img class="icon" src="<?php echo engine::$icon; ?>">
<h1><?php echo engine::$title; ?></h1>
</a>
</div>
<div class="menu center">
<ul>
<li><?php echo implode('</li><li>', array_map(function ($item) {
        return '<a href="' . engine::alias($item['alias'], true) . '">' . $item['title'] . '</a>';
    }, engine::$menu)); ?></li>
</ul>
</div>
<div class="placeholder center"></div>
<div class="content center">
<h2><?php echo engine::$item['title']; ?></h2>
<?php echo engine::$item['description']; ?>
<?php echo engine::load(); ?>
</div>
<div class="footer center">
<ul class="apps">
<?php
foreach (engine::$locale as $alias => $app) :
    if ($alias == 'rizkysly' || engine::$alias == 'rizkysly') continue; ?>
<a href="/<?php echo $alias; ?>">
    <img class="icon" src="<?php echo $app['icon']; ?>">
    <?php echo $app['title']; ?>
</a>
<?php endforeach; ?>
</ul>
<ul class="domains">
<li><?php echo implode('</li><li>', array_map(function ($domain) {
        return '<a href="https://' . $domain . '">' . $domain . '</a>';
    }, engine::$domains)); ?></li>
</ul>
</div>
<script src="/js/main.js"></script>
</body>
</html>