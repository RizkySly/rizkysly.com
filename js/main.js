// Make sure this script is loaded after all dom elements

window.addEventListener('scroll', function () {
    console.log('Hoi');
    var el = document.querySelector('.menu');
    if (el.classList.contains('scroll')) {
        el = document.querySelector('.placeholder');
    }
    var bounding = el.getBoundingClientRect();
    if (bounding.y < 0) {
        document.querySelector('.menu').classList.add('scroll');
        document.querySelector('.placeholder').classList.add('scroll');
    }
    else {
        document.querySelector('.menu').classList.remove('scroll');
        document.querySelector('.placeholder').classList.remove('scroll');
    }
});